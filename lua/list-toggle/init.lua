local toggle_list = function(is_loclist)
  local prefix = is_loclist and 'l' or 'c'
  local tabnr = vim.fn.tabpagenr()

  for _, win in ipairs(vim.fn.getwininfo()) do
    if win.tabnr == tabnr and win.quickfix == 1 and win.loclist == (is_loclist and 1 or 0) then
      vim.cmd(prefix .. 'close')
      return
    end
  end

  local ok, err = pcall(function()
    vim.cmd(prefix .. 'open')
  end)

  if not ok then
    vim.notify('Error: ' .. err)
  end
end

return {
  setup = function () end,
  toggle_quickfix = function ()
    toggle_list(false)
  end,
  toggle_loclist = function ()
    toggle_list(true)
  end,
}
